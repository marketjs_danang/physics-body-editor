ig.module('game.entities.editor-controller')
    .requires(
        'impact.entity',
        'game.entities.buttons.generate-vertices-bt',
        'game.entities.vertice',
        'game.entities.buttons.delete-bt',
        'game.entities.buttons.add-bt'
    )
    .defines(function () {
        EntityEditorController = ig.Entity.extend({
            zIndex: 1,

            img: new ig.Image("media/graphics/misc/invisible.png"),
            vertices: [],
            selectedVertice: null,

            init: function (x, y, settings) {
                this.parent(x, y, settings);
                ig.game.spawnEntity(EntityGenerateVerticesBt, 0, 0, {
                    zIndex: this.zIndex + 1,
                    controller: this,
                    fixedToCamera: true
                });

                ig.game.spawnEntity(EntityDeleteBt, 0, 0, {
                    zIndex: this.zIndex + 1,
                    controller: this,
                    fixedToCamera: true
                });

                ig.game.spawnEntity(EntityAddBt, 0, 0, {
                    zIndex: this.zIndex + 1,
                    controller: this,
                    fixedToCamera: true
                });

                this.center = {};
                this.center.x = ig.system.width / 2;
                this.center.y = ig.system.height / 2;
                if (MY_IMAGE == null) MY_IMAGE = this.img.data;


                this.savePointer = {};
                this.saveScreen = {};
                this.shouldMove = false;
                this.isClicked = false;

                ig.game.sortEntitiesDeferred();
            },

            spawnVertices: function () {
                // empty vertices array
                if (this.vertices.length > 0) {
                    for (var i = 0, len = this.vertices.length; i < len; i++) {
                        this.vertices[i].kill();
                    }
                    this.vertices.splice(0);
                    this.selectedVertice = null;
                }

                // spawn original vertices
                this.vertices[0] = ig.game.spawnEntity(EntityVertice, this.center.x - MY_IMAGE.width / 2, this.center.y - MY_IMAGE.height / 2, {
                    zIndex: this.zIndex + 1,
                    controller: this
                });
                this.vertices[1] = ig.game.spawnEntity(EntityVertice, this.center.x + MY_IMAGE.width / 2, this.center.y - MY_IMAGE.height / 2, {
                    zIndex: this.zIndex + 1,
                    controller: this
                });
                this.vertices[2] = ig.game.spawnEntity(EntityVertice, this.center.x + MY_IMAGE.width / 2, this.center.y + MY_IMAGE.height / 2, {
                    zIndex: this.zIndex + 1,
                    controller: this
                });
                this.vertices[3] = ig.game.spawnEntity(EntityVertice, this.center.x - MY_IMAGE.width / 2, this.center.y + MY_IMAGE.height / 2, {
                    zIndex: this.zIndex + 1,
                    controller: this
                });
            },

            update: function () {
                this.parent();
                if (MY_ISLOADED) {
                    ig.game.screen.x = ig.game.screen.y = 0;
                    this.spawnVertices();
                    MY_ISLOADED = false;
                }
                if (ig.input.pressed("ctrl") && !this.isClicked) {
                    ig.domHandler.getElementById("#canvas").css("cursor", "pointer");
                    this.shouldMove = true;

                }
                if (ig.input.released("ctrl")) {
                    if (this.shouldMove) this.shouldMove = false;
                    ig.domHandler.getElementById("#canvas").css("cursor", "default");
                }

                if (ig.input.pressed("click")) {
                    this.isClicked = true;
                    if (this.shouldMove) {
                        var pos = ig.game.io.getClickPos();
                        this.savePointer.x = pos.x; // - ig.game.screen.x;
                        this.savePointer.y = pos.y; // - ig.game.screen.y;
                        this.saveScreen.x = ig.game.screen.x; // - ig.game.screen.y;
                        this.saveScreen.y = ig.game.screen.y; // - ig.game.screen.y;}
                    }
                }

                if (ig.input.state("click") && this.shouldMove) {
                    var pos = ig.game.io.getClickPos();
                    var dx = pos.x - this.savePointer.x;
                    var dy = pos.y - this.savePointer.y;
                    ig.game.screen.x = this.saveScreen.x - dx;
                    ig.game.screen.y = this.saveScreen.y - dy;
                }

                if (ig.input.released("click")) {
                    if (this.isClicked) this.isClicked = false;
                }
            },

            draw: function () {
                ig.system.context.fillStyle = BG_COLOR;
                ig.system.context.fillRect(0, 0, ig.system.width, ig.system.height);
                this.parent();

                // draw image
                ig.system.context.save();
                ig.system.context.translate(this.center.x - ig.game.screen.x, this.center.y - ig.game.screen.y);
                ig.system.context.scale(X_FLIP * MY_ZOOM, Y_FLIP * MY_ZOOM);
                ig.system.context.drawImage(
                    MY_IMAGE, -MY_IMAGE.width / 2, -MY_IMAGE.height / 2, MY_IMAGE.width, MY_IMAGE.height
                );
                ig.system.context.restore();


                // draw polygons
                if (this.vertices.length == 0) return;
                ig.system.context.save();
                ig.system.context.beginPath();
                ig.system.context.moveTo(this.vertices[0].getCenter().x - ig.game.screen.x, this.vertices[0].getCenter().y - ig.game.screen.y);
                for (var i = 1, len = this.vertices.length; i < len; i++) {
                    ig.system.context.lineTo(this.vertices[i].getCenter().x - ig.game.screen.x, this.vertices[i].getCenter().y - ig.game.screen.y);
                }
                ig.system.context.lineTo(this.vertices[0].getCenter().x - ig.game.screen.x, this.vertices[0].getCenter().y - ig.game.screen.y);
                ig.system.context.closePath();
                // fill and stroke
                ig.system.context.fillStyle = "rgba(0,0,255,0.3)";
                ig.system.context.fill();
                ig.system.context.strokeStyle = "#F00";
                ig.system.context.stroke();
                ig.system.context.restore();
            }

        });

    });
