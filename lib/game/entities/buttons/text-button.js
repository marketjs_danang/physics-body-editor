ig.module('game.entities.buttons.text-button')
    .requires(
        'impact.entity'
    )
    .defines(function () {
        EntityTextBt = ig.Entity.extend({
            type: ig.Entity.TYPE.A,

            size: {
                x: 200,
                y: 30,
            },

            controller: null,

            text: "Generate vertices",
            font: "bold 17px Arial",
            isClicked: false,

            init: function (x, y, settings) {
                this.parent(x, y, settings);
                this.pos.x = (ig.system.width - this.size.x) / 2;
                this.pos.y = ig.system.height - this.size.y - 15;
                this.ctx = ig.system.context;
                this.style = "#F7FF63";
                this.center = this.getCenter();
                this.dy = 0;
            },

            draw: function () {
                this.parent();
                this.ctx.save();
                this.ctx.fillStyle = this.style;
                this.ctx.strokeStyle = "#FF5500";
                this.ctx.lineWidth = 6;
                this.roundRect(this.ctx, this.center.x - this.size.x/2, this.center.y - this.size.y/2 + this.dy, this.size.x, this.size.y, 6, true, true);
                this.ctx.font = this.font;
                this.ctx.textBaseline = "middle";
                this.ctx.fillStyle = "#25691E";
                this.ctx.textAlign = "center";
                this.ctx.fillText(this.text, this.center.x, this.center.y + this.dy);
                this.ctx.restore();
            },
            clicked: function () {
                this.isClicked = true;
                this.dy = 2;
                this.style = "#B5FFC4";
            },
            clicking: function () {

            },
            released: function () {
                if (this.isClicked) {
                    this.dy = 0;
                    this.style = "#F7FF63";
                    this.isClicked = false;
                }
            },

            over: function () {
                this.style = "#FFD4B8";
            },
            leave: function () {
                this.style = "#F7FF63";
            },
            releasedOutside: function () {
                if (this.isClicked) {
                    this.dy = 0;
                    this.style = "#F7FF63";
                    this.isClicked = false;
                }
            },
            getCenter: function () {
                return {
                    x: this.pos.x + this.size.x / 2,
                    y: this.pos.y + this.size.y / 2
                }
            },
            roundRect: function (ctx, x, y, width, height, radius, fill, stroke) {
                if (typeof stroke == "undefined") {
                    stroke = true;
                }
                if (typeof radius === "undefined") {
                    radius = 5;
                }
                ctx.beginPath();
                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);
                ctx.closePath();
                if (stroke) {
                    ctx.stroke();
                }
                if (fill) {
                    ctx.fill();
                }
            }
        });
    });
