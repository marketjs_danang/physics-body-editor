ig.module('game.entities.buttons.add-bt')
    .requires(
        'game.entities.buttons.text-button'
    )
    .defines(function () {
        EntityAddBt = EntityTextBt.extend({
            size: {
                x: 100,
                y: 25
            },

            text: "Add vertex",
            font: "bold 14px Arial",

            init: function (x, y, settings) {
                this.parent(x, y, settings);
                this.pos.x = ig.system.width - this.size.x - 10;
                this.pos.y = ig.system.height - (this.size.y + 18) * 2;
                this.center = this.getCenter();
            },

            released: function () {
                if (!this.isClicked) return;
                this.parent();

                var vertices = this.controller.vertices;
                var len = vertices.length;
                if (len == 0) return;

                var selectedVertice = this.controller.selectedVertice;
                if (!selectedVertice) {
                    var v1 = vertices[len - 1];
                    var v2 = vertices[0];
                    if (v1.distanceTo(v2) < 2 * v2.size.x) {
                        alert("The distance is too small to add vertex!!!");
                        return;
                    }
                    var x = v2.pos.x + (v1.pos.x - v2.pos.x) / 2 + v2.size.x / 2;
                    var y = v2.pos.y + (v1.pos.y - v2.pos.y) / 2 + v2.size.y / 2;
                    var vertice = ig.game.spawnEntity(EntityVertice, x, y, {
                        zIndex: this.controller.zIndex + 1,
                        controller: this.controller
                    });
                    vertices.push(vertice);
                } else {
                    var v1 = selectedVertice;
                    var no = vertices.indexOf(selectedVertice);
                    if (no < len - 1) {
                        var v2 = vertices[no + 1];
                    } else {
                        var v2 = vertices[0];
                    }

                    if (v1.distanceTo(v2) < 2 * v2.size.x) {
                        alert("The distance is too small to add vertex!!!");
                        return;
                    }
                    var x = v2.pos.x + (v1.pos.x - v2.pos.x) / 2 + v2.size.x / 2;
                    var y = v2.pos.y + (v1.pos.y - v2.pos.y) / 2 + v2.size.y / 2;
                    var vertice = ig.game.spawnEntity(EntityVertice, x, y, {
                        zIndex: this.controller.zIndex + 1,
                        controller: this.controller
                    });
                    vertices.splice(no + 1, 0, vertice);
                }
            }
        });
    });
