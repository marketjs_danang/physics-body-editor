/**
 *  Repos - Only use when there is both portrait and landscape mode for the mobile version.
 *  Place one in any level that mobile orientation repositioning is needed
 *
 *  Created by Isaac Choi on 2013-04-09.
 *  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
 */

ig.module('game.entities.vertice')
    .requires(
        'impact.entity'
    )
    .defines(function () {
        EntityVertice = ig.Entity.extend({
            type: ig.Entity.TYPE.A,
            size: {
                x: 12,
                y: 12
            },
            isClicked: false,
            mouseOffset: {
                x: 0,
                y: 0
            },
            isSelected: false,
            controller: null,

            init: function (x, y, settings) {
                this.parent(x, y, settings);
                this.style = "rgba(0,255,0,0.5)";
                this.center = {};
                this.center.x = ig.system.width / 2;
                this.center.y = ig.system.height / 2;
                this.pos.x -= this.size.x / 2;
                this.pos.y -= this.size.y / 2;

                this.lastZoom = MY_ZOOM;

                ig.game.sortEntitiesDeferred();
            },

            draw: function () {
                this.parent();
                ig.system.context.fillStyle = this.style;
                ig.system.context.fillRect(this.pos.x - ig.game.screen.x, this.pos.y - ig.game.screen.y, this.size.x, this.size.y);
                ig.system.context.strokeStyle = "#00F";
                ig.system.context.strokeRect(this.pos.x - ig.game.screen.x, this.pos.y - ig.game.screen.y, this.size.x, this.size.y);
            },
            clicked: function () {
                if(this.controller.shouldMove) return;
                this.isClicked = true;
                this.style = "rgba(255,0,0,0.4)";
                this.mouseOffset.x = this.pos.x - ig.game.io.getClickPos().x;
                this.mouseOffset.y = this.pos.y - ig.game.io.getClickPos().y;
            },
            clicking: function () {
                if (this.isClicked) {
                    this.pos.x = ig.game.io.getClickPos().x + this.mouseOffset.x;
                    this.pos.y = ig.game.io.getClickPos().y + this.mouseOffset.y;
                }
            },
            released: function () {
                if(this.controller.shouldMove) return;
                if (this.isClicked) {
                    this.isClicked = false;
                    if (this.isSelected) {
                        this.isSelected = false;
                        this.style = "rgba(0,255,0,0.5)";
                        this.controller.selectedVertice = null;
                    } else {
                        this.isSelected = true;
                        if (this.controller.selectedVertice) {
                            this.controller.selectedVertice.isSelected = false;
                            this.controller.selectedVertice.style = "rgba(0,255,0,0.5)";
                        }
                        this.controller.selectedVertice = this;
                    }
                }

            },
            releasedOutside: function () {
                this.released();
            },

            over: function () {
                this.style = "rgba(255,255,0,0.5)";
            },
            leave: function () {
                if (this.isSelected) this.style = "rgba(255,0,0,0.4)";
                else this.style = "rgba(0,255,0,0.5)";
            },

            getCenter: function () {
                return {
                    x: this.pos.x + this.size.x / 2,
                    y: this.pos.y + this.size.y / 2
                }
            },

            getCenterPos: function () {
                return {
                    x: this.getCenter().x - this.center.x,
                    y: this.getCenter().y - this.center.y
                }
            },

            update: function () {
                this.parent();
                if (this.lastZoom != MY_ZOOM) {
                    this.pos = this.getOriginalPos(this.lastZoom);
                    this.pos = this.getZoomedPos();
                    this.lastZoom = MY_ZOOM;
                }

            },

            getOriginalPos: function (zoom) {
                return {
                    x: this.getCenterPos().x / zoom + this.center.x - this.size.x / 2,
                    y: this.getCenterPos().y / zoom + this.center.y - this.size.y / 2
                }
            },

            getZoomedPos: function () {
                return {
                    x: this.getCenterPos().x * MY_ZOOM + this.center.x - this.size.x / 2,
                    y: this.getCenterPos().y * MY_ZOOM + this.center.y - this.size.y / 2
                }
            },

            getVerticePos: function () {
                var pos = this.getOriginalPos(this.lastZoom);
                var x = (pos.x + this.size.x / 2 - this.center.x) / 10;
                var y = (pos.y + this.size.y / 2 - this.center.y) / 10;
                return {
                    x: x.toFixed(2),
                    y: y.toFixed(2)
                }
            }
        });

    });
