ig.module('game.entities.buttons.generate-vertices-bt')
    .requires(
        'game.entities.buttons.text-button'
    )
    .defines(function () {
        EntityGenerateVerticesBt = EntityTextBt.extend({

            init: function (x, y, settings) {
                this.pos.x = (ig.system.width - this.size.x) / 2;
                this.pos.y = ig.system.height - this.size.y - 15;
                this.parent(x, y, settings);
            },

            released: function () {
                if (!this.isClicked) return;
                this.parent();
                var s = "vertices: [";
                var vertices = this.controller.vertices;
                for (var i = 0, len = vertices.length; i < len; i++) {
                    var pos = vertices[i].getVerticePos();
                    s += "{x: " + pos.x + ", y: " + pos.y + "},";
                }
                s = s.slice(0, -1);
                s += "],";
                alert(s);
            }
        });
    });
