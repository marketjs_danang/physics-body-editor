ig.module('game.levels.editor')
    .requires('impact.image', 'game.entities.editor-controller')
    .defines(function () {
        LevelEditor = /*JSON[*/ {
            "entities": [{
                "type": "EntityEditorController",
                "x": 0,
                "y": 0
            }],
            "layer": []
        } /*]JSON*/ ;

    });
