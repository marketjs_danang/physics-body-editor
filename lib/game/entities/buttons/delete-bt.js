ig.module('game.entities.buttons.delete-bt')
    .requires(
        'game.entities.buttons.text-button'
    )
    .defines(function () {
        EntityDeleteBt = EntityTextBt.extend({
            size: {
                x: 100,
                y: 25
            },

            text: "Delete vertex",
            font: "bold 14px Arial",

            init: function (x, y, settings) {
                this.parent(x, y, settings);
                this.pos.x = ig.system.width - this.size.x - 10;
                this.pos.y = ig.system.height - (this.size.y + 18);
                this.center = this.getCenter();
            },

            released: function () {
                if (!this.isClicked) return;
                this.parent();

                var selectedVertice = this.controller.selectedVertice;
                if (!selectedVertice) return;

                var vertices = this.controller.vertices;
                if (vertices.length == 3) {
                    alert("Minimum 3 vertices on screen!!!");
                    return;
                }
                var n = vertices.indexOf(selectedVertice);
                vertices.splice(n, 1);
                selectedVertice.kill();
                this.controller.selectedVertice = null;
            }
        });
    });
